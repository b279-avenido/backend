// Use "require" directive to load Node.js modules
// A "http module" lets Node.js tranfer data using hyper test transfer protocol (http)
// clients (browser) and servers (node JS/express JS application) communicate by exchanging individual message.

let http = require("http");

// using this modules create server method we can create http server that listents to request on specified port.
// A port is a virtual point where network connection start and end.
// Each port is associated with a specific process or service
// The server will be assigned to port 400
http.createServer(function (request, response){
	// we use writeHead() method to:
	// set a status code for the message - 200 means ok
	// Set the content-type of the response as a plain text
	response.writeHead(200, {"Content-Type" : "text/plain"});
	response.end("Hello world");
}).listen(4000);

console.log("server running at localhost:4000");

