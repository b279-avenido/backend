let http = require("http");

http.createServer(function (req, res){
	// CRUD --> R --> RETRIEVE
	// GET Method is somehow equal to Retieve method in CRUD or read a data
	if(req.url == "/items" && req.method == "GET"){
		res.writeHead(200, {"Content-Type" : "text/plain"});
		res.end("Data retrieved from the database.")
	}

// POST METHOD is used for creating/sending data to the database
if(req.url == "/items" && req.method == "POST"){
		res.writeHead(200, {"Content-Type" : "text/plain"});
		res.end("Data to be sent to the database")
	}

}).listen(4000);

console.log("Server running at localhost:4000");

