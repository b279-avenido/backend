//Important Note: Do not change the variable names. 
//All required classes, variables and function names are listed in the exports.

// Exponent Operator
const getCube = 2 ** 3;


// Template Literals

console.log(`The cube of 2 is ${getCube}.`);

// Array Destructuring
const address = ["258", "Washington Ave NW", "California", "90011"];
console.log(address[0]);
console.log(address[1]);
console.log(address[2]);
console.log(address[3]);
console.log(`I live in ${address[0]} ${address[1]}, ${address[2]} ${address[3]}`)



// Object Destructuring
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}
console.log(`${animal.name} was a ${animal.species}, He weighed at ${animal.weight}, with a mesurement of ${animal.measurement}`);


// Arrow Functions


let numbers = [1, 2, 3, 4, 5];
numbers.forEach((number) => console.log(number));
let reduceNumber = numbers.reduce((num, numb) => num + numb);
console.log(reduceNumber);


// Javascript Classes

class Dog{
    constructor(name, age, breed){
        this.name = "Frankie";
        this.age = 5;
        this.breed = "Miniature Dachshund";

    }
} 
const myDog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(myDog);


//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        getCube: typeof getCube !== 'undefined' ? getCube : null,
        houseNumber: typeof houseNumber !== 'undefined' ? houseNumber : null,
        street: typeof street !== 'undefined' ? street : null,
        state: typeof state !== 'undefined' ? state : null,
        zipCode: typeof zipCode !== 'undefined' ? zipCode : null,
        name: typeof name !== 'undefined' ? name : null,
        species: typeof species !== 'undefined' ? species : null,
        weight: typeof weight !== 'undefined' ? weight : null,
        measurement: typeof measurement !== 'undefined' ? measurement : null,
        reduceNumber: typeof reduceNumber !== 'undefined' ? reduceNumber : null,
        Dog: typeof Dog !== 'undefined' ? Dog : null

    }
} catch(err){

}