// console.log("hello")

// section - Exponent operator

const firstNum = 8 ** 2;
console.log(firstNum);

const secondNum = Math.pow(8, 2);
console.log(secondNum);

// section - Template literals
/*
	- Allows to write strings without using the concatenation operator (+)
	- Greatly helps with code readability
*/
// pre-template literal strings
let name = "John";
let message = "hello " + name + "! welcome to programming"
console.log("Message without template literals: " + message);


// using template literals
// Backticks (``)
message = `hello ${name}! welcome to programming`;
console.log(`message with template literals: ${message}`);

// Multi line using template literal
const anotherMessage = `
${name} attended math compitetion
He won it the solving the problem 8 ** 2 with the solution of ${firstNum};
`
console.log(anotherMessage);

const interestRate = .1;
const principal = 1000;
console.log(`The intereest on your savings acounnt is" ${principal * interestRate}`);


// section - Pre-array Destructuring
/*
	- Allows to unpack elements in arrays into distinct variables
	- Allows us to name array elements with variables instead of using index numbers
	- Helps with code readability
	- Syntax
		let/const [variableName, variableName, variableName] = array;
*/

const fullName = ["juan", "bella", "cruz"];
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);
console.log(`hello ${fullName[0]} ${fullName[1]} ${fullName[0]}! it nice to meet you`);

// ARRAY DEstructuring
const [firstName, middle, lastName] = fullName;
console.log(`hello ${firstName} ${middle} ${lastName}! it nice to meet you`);



// Section - Object destructuring
/*
	- Allows to unpack properties of objects into distinct variables
	- Shortens the syntax for accessing properties from objects
	- Syntax
		let/const {propertyName, propertyName, propertyName} = object;
*/

const person = {
	givenName: "jane",
	maidenName: "Dela",
	familyName: "crus"
}
// pre-object destruction
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);
console.log(`hello ${person.givenName} ${person.maidenName} ${person.familyName}! is good to see you again`);

// object destructuring
const{givenName, maidenName, familyName} = person;
console.log(`hello ${givenName} ${maidenName} ${familyName}! is good to see you again.`);

function getFullName({givenName, maidenName, familyName}){
	console.log(`${givenName} ${maidenName} ${familyName}`);
}
getFullName(person);


// Section - arrow function

/*
	- Compact alternative syntax to traditional functions
	- Useful for code snippets where creating functions will not be reused in any other portion of the code
	- Adheres to the "DRY" (Don't Repeat Yourself) principle where there's no longer need to create a function and think of a name for functions that will only be used in certain code snippets
*/

/*
const variableName = () => {
	console.log()
}
*/

const hello = () => {
	console.log("hello");
}

const printFullName = (firstName, middle, lastName) => {
	console.log(`${firstName} ${middle} ${lastName}`);
}
printFullName("John", "d", "sed");



const students = ["jane", "joe", "juan"];
// arrow function with loops

// pre-arrow function
students.forEach(function(students){
	console.log(`${students} is a student.`);
})

// using arrow function
students.forEach((students) => {
	console.log(`${students} is a student.`);
});



// section - Implicit return statement
/*
	- There are instances when you can omit the "return" statement
	- This works because even without the "return" statement JavaScript implicitly adds it for the result of the function
*/

// arrow fuction
const add = (num1, num2) => num1 + num2;
let total = add(1, 2);
console.log(total);


// Section - Default function argument value
const greet = (name = "user") => {
	return `Good morning, ${name}!`
}

console.log(greet());
console.log(greet("John"));



// Section - Clsass based object blueprint
class Car {
	constructor(brand, name, year) {
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}
const myCar = new Car();
console.log(myCar);
myCar.brand = "ford";
myCar.name = "ranger";
myCar.year = 2021;
console.log(myCar);

const myNewCar = new Car("toyota", "vios", 2021);
console.log(myNewCar);