db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", fruitsOnSale: {$sum: 1}}},
	{$project: {_id: 0}}
	
]);

// 2.
db.fruits.aggregate([
	{$match: {stock: {$gte: 20}}},
	{$group: {_id: "supplier_id", enoughStock: {$sum: 1}}},
	{$project: {_id: 0}}
]);


// 3.

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", ave_price: {$avg: "$price"}}},
	{$sort: {avg: -1}}
]);



// 4.
db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", max_price: {$max: "$price"}}},
	{$sort: {max_price: 1}}
]);

// 5.
db.fruits.aggregate([
 	{$match: {onSale: true}},
 	{$group: {_id: "$supplier_id", min_price: {$min: "$price"}}},
 	{$sort: {min_price: -1}}
]);
