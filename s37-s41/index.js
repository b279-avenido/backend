// server creation and DB creation
const express = require("express");
const mongoose = require("mongoose");

// allows us to control the apps cross origin resource sharing
const cors = require("cors");
const userRoutes = require("./routes/user.js");
const courseRoutes = require("./routes/course.js");


const app = express();



// mongoDB connection using SRV link
mongoose.connect("mongodb+srv://admin:admin123@zuitt-bootcamp.wwop0oq.mongodb.net/Course_Booking_System?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

// optional - validation of DB connection
mongoose.connection.once("open", () => console.log("Now connected to mongDB Atlas."))




// middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));


// defines the "/users" string to ne included for the user routes defined in the "user.js" route file.
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);





// port listening
if(require.main === module){
	app.listen(process.env.PORT || 4000, () => console.log(`API is now online on post ${process.env.PORT || 4000}`));
}

module.exports = app;