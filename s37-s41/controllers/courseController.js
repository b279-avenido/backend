const Course = require("../models/Course.js");

// Create a new course
/*module.exports.addCourse = (reqBody) => {
	let newCourse = new Course({
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	})
	// save the creating object into the db
	return newCourse.save().then((course, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}*/
// activity 39
module.exports.addCourse = (data) => {
	console.log(data.isAdmin);
	if(data.isAdmin) {
		let newCourse = new Course({
			name: data.course.name,
			description: data.course.description,
			price: data.course.price
		})

		return newCourse.save().then((course, error) => {
			if(error){
				return error;
			}
			return course;
		})
	} 

	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
}

/*module.exports.addAdmin = (admin) => {
	if(admin == {isAdmin: true}){
		let newCourse = new Course({
			name : reqBody.name,
			description : reqBody.description,
			price : reqBody.price
			isAdmin : true;
		})
		return newCourse.save().then((result, error) => {
			if(error){
				return false;

			}else{
				return true;
			}
		})

	}else{
		return "Not authorize to create course"
	}
}*/


// get all courses function
module.exports.getAllCourse = () =>{
	return Course.find({}).then(result => {
		return result;
	})
}


// get all courses function
module.exports.getAllActive = () =>{
	return Course.find({isActive : true}).then(result => {
		return result;
	})
}

/*module.exports.getAllActive = () =>{
	return Course.find({isActive : false}).then(result => {
		return result;
	})
}*/


// retrieving specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})
}

// Update a course
// module.exports.updateCourse = (reqParams, reqBody, isAdmin) => {
// 	console.log(isAdmin);

// 	if(isAdmin){
// 		let updatedCourse = {
// 		name : reqBody.name,
// 		description : reqBody.description,
// 		price : reqBody.price
// 		}

// 		return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
// 			if(error){
// 				return false;
// 			}else{
// 				return "Course Updated!";
// 			}
// 		})
// 	}


// 	let message = Promise.resolve("You don't have the access rights to do this action.");

// 	return message.then((value) => {
// 		return value
// 	})

	
// }

// activity 40

module.exports.eraseCourse = (reqParams, reqBody, isAdmin) => {
	console.log(isAdmin);
	if(isAdmin){
		let deleteCourse= {
			isActive: reqBody.isActive
		}

		return Course.findByIdAndUpdate(reqParams.courseId, deleteCourse).then((course, error) => {
		if(error){
			return false;
		}else{
			return "Course has been deleted"
		}
	})
	}
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})

}

