const express = require("express");
const router = express.Router();
const userController = require("../controllers/user.js");
const auth = require("../auth.js")


// route for checking if email already exists
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController =>
		res.send(resultFromController));
});

// route for registering
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController =>
		res.send(resultFromController));
});

// route for user login
router.post("/login", (req, res) =>{
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

/*router.post("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	userController.detailsUser(userData.id).then(resultFromController => res.send(resultFromController))
});*/


// Route for retrieving user details
router.post("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	// Provides the user's ID for the getProfile controller method
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});


/*// activity option
router.post("/details", (req, res) => {
	userController.detailsUser(req.body).then(resultFromController => res.send(resultFromController))
});*/
/*router.post("/details", (req,res)=>{
    userController.userDetails(req.body).then(resultFromController => res.send(resultFromController))
});*/


// enroll user to a course
/*router.post("/enroll", (req, res) => {
	let data = {
		userId : req.body.userId,
		courseId : req.body.courseId
	}
	userController.enroll(data).then(resultFromController => res.send(resultFromController))
});*/




// activity 41

router.post("/enroll", (req, res) => {
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		courseId : req.body.courseId,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	userController.enroll(data).then(resultFromController => res.send(resultFromController));
})








// allows us to export the "router" object that will be access in our index.js
module.exports = router;