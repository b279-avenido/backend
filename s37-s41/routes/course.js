const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController.js");
const auth = require("../auth.js");


/*// Route for creating course
router.post("/", (req, res) => {
	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))

});*/

// activity 39
// Route for creating a course 
router.post("/", auth.verify, (req, res) => {
	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	courseController.addCourse(data).then(resultFromController => res.send(resultFromController));
});


/*router.post("/", (req, res) =>{
	courseController.addAdmin(admin).then(resultFromController => res.send(resultFromController))
})
*/


// get all courses
router.get("/all", (req, res) =>{
	courseController.getAllCourse().then(resultFromController => res.send(resultFromController));
});


// get all active courses

router.get("/", (req, res) =>{
	courseController.getAllActive().then(resultFromController => res.send(resultFromController));
});


// retrieve specific course
router.get("/:courseId", (req, res) => {
	console.log(req.params.courseId);
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
});

// update specific course
// Update a specific course
/*router.put("/:courseId", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	courseController.updateCourse(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));

});*/

// activity 40
// router.put("/:courseId", auth.verify, (req, res) => {
// 	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
// 	courseController.eraseCourse(req.params, req.body, isAdmin).the(resultFromController => res.send(resultFromController));

// })
router.put("/:courseId", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	courseController.eraseCourse(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));

});







// allows us to export the outer object that will be access in our index.js
module.exports = router;