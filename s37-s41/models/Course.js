const mongoose = require("mongoose");
const courseSchema = new mongoose.Schema({
	name: {
		type: String,
		required : [true, "Course Name is Required"]
	},
	description : {
		type: String,
		required : [true, "Course Description is Required"]
	},
	price : {
		type: Number,
		required : [true, "Course Price is Required"]
	},
	isActive : {
		type: Boolean,
		default : true
	},
	createdOn : {
		type: Date,
		// The "new date" expression instantiates
		default : new Date()
	},
	enrollees : [
	{
		userId : {
			type : String,
			required : [true, "User ID is Required!"]
		},
		enrolledOn : {
			type : Date,
		default : new Date()
		}
	}
		]
	
})

module.exports = mongoose.model("Course", courseSchema);