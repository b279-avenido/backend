const express = require("express");
// allows access to http method middleware that makes easier to create routes for our application
const router = express.Router();
const taskController = require("../controllers/taskController.js");


// ?section - Routes
// Routes are responsible for defining the URI that our client accessses with corresponding controllers

// Route to get all tasks
router.get("/", (req, res) => {
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
});

// ?route to create a new task
router.post("/", (req, res) =>{
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
});




// delete specific task
// The whole URL is at "http://localhost:4000/tasks/:id"
// The task ID is obtained from the URL is denoted by the ":id" identifier in the route
// The colon (:) is an identifier that helps create a dynamic route which allows us to supply information in the URL
// The word that comes after the colon (:) symbol will be the name of the URL parameter
// ":id" is a wildcard where you can put any value, it then creates a link between "id" parameter in the URL and the value provided in the URL

router.delete("/:id", (req, res) =>
{
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
});

// Updating a task
router.put("/:id", (req, res) =>{
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
});

// acitvity

router.get("/:id", (req, res) => {
	taskController.getUser(req.params.id, req.body.username).then(result => res.send(result));
});

router.put("/:id/complete", (req, res) => {
	taskController.updateTask(req.params.id, req.body.status).then(result => res.send(result));
})

// router.put("/:id/complete", (req, res) => {
// 	taskController.updateTask(req.params.id, req.body).then(result => res.send(result))
// });


module.exports = router;