// Set up the dependencies/packages
const express = require("express");
const mongoose = require("mongoose");

const taskRoute = require("./routes/taskRoute.js");


// Server set up/middlewares
const app = express();
const port = 4000;
app.use(express.json())
app.use(express.urlencoded({extended: true}));


// Add task route
// allows all the task route created inthe taskRoute.js file to use /task
app.use("/tasks", taskRoute);



// DB connection
// connecting to the mongoDB atlas
mongoose.connect("mongodb+srv://admin:admin123@zuitt-bootcamp.wwop0oq.mongodb.net/B279_to-do?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology: true
});
















// server listening
if(require.main === module){
	app.listen(port, () => console.log(`Server running at port ${port}`));
}
module.exports = app;