const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
    name : {
        type : String,
        required : [true, "Product name is required!"],
        unique: true 
    },
    description : {
        type : String,
        required : [true, "Product description is required!"]
    },
    category : {
        type : String,
        required : [true, "Product category is required!"]
    },
    image : {
        type : String,
        // required : [true, "Product image is required!"]
    },
    price : {
        type : Number,
        required : [true, "Product price is required!"]
    },

    trending: {
        type: Boolean,
        default: false
      },

    isActive : {
        type : Boolean,
        default: true
    },
    stocks : {
        type: Number,
        default: 100,
    },

    createdOn : {
        type : Date,
        default: new Date()
    },
})

module.exports = mongoose.model("Product", productSchema);

