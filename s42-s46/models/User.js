const mongoose = require("mongoose");
const bcrypt = require('bcrypt');
const userSchema = new mongoose.Schema({
    username : {
        type : String,
        required : [true, "Username is required!"],
        unique: true 
    },
    email : {
        type : String,
        required : [true, "Email is required!"]
    },
    password : {
        type : String,
        required : [true, "Password is required!"]
    },
    isAdmin : {
        type : Boolean,
        default: false
    },
    verified : {
        type : Boolean,
        default: false,
    },
    balance : {
        type: Number,
        default: 0,
    },
    
})


userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
  };

module.exports = mongoose.model("User", userSchema);