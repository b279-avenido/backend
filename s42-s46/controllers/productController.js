const Order = require("../models/Order")
const Product = require("../models/Product")
const mongoose = require('mongoose');


// Create a new product
module.exports.addProduct = (data) => {
	console.log(data.isAdmin);
	if(data.isAdmin) {
		let newProduct = new Product({
			name: data.product.name,
			description: data.product.description,
			category: data.product.category,
			price: data.product.price,
			image: data.product.image
		})
		return newProduct.save().then((product, error) => {
			if(error){
				return error;
			}
			return product;
		})
	}

	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve("You don't have the access rights to do this action.");
	return message.then((value) => {
		return value
	})
}

// Retreiving all product
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	})
}

// Retreiving all active products
module.exports.getAllActive = () => {
	return Product.find({isActive : true}).then(result => {
		return result;
	})
}

// Retreiving a specific product
module.exports.getProduct = (reqParams) => {
	// Check if the provided productId is valid
	if (!mongoose.Types.ObjectId.isValid(reqParams.productId)) {
		return Promise.resolve(`No product ID found for ID: ${reqParams.productId}`);
	}else{	
		return Product.findById(reqParams.productId).then(result =>{
			return result;
		})
	}
}


// Update a product
module.exports.updateProduct = (reqParams, reqBody, isAdmin) => {
	console.log(isAdmin);

	// Check if the provided productId is valid
	if (!mongoose.Types.ObjectId.isValid(reqParams.productId)) {
		return Promise.resolve(`No product ID found for ID: ${reqParams.productId}`);
	}

	if(isAdmin){
		let updatedProduct = {
		name : reqBody.name,
		description : reqBody.description,
		category: reqBody.category,
		price : reqBody.price,
		category : reqBody.category,
		trending : reqBody.trending,
		isActive : reqBody.isActive,
		image : reqBody.image
		}

		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
			if(error){
				return false;
			}else{
				return "Product Updated!";
			}
		})
	}	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})	
}

// Archive a product
module.exports.archiveProduct = (reqParams, isAdmin) => {
	console.log(isAdmin);

	// Check if the provided productId is valid
	if (!mongoose.Types.ObjectId.isValid(reqParams.productId)) {
		return Promise.resolve(`No product ID found for ID: ${reqParams.productId}`);
	}
		
	let updateActiveField = {
		isActive : false
	};

	if(isAdmin){
		return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {

			// Product not archived
			if (error) {
				return false
				// false
			// Product archived successfully
			} else {
				return true
				//true
			}
		});	
	}

	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
};

// Activate a product
module.exports.activateProduct = (reqParams, isAdmin) => {
	console.log(isAdmin);

	// Check if the provided productId is valid
	if (!mongoose.Types.ObjectId.isValid(reqParams.productId)) {
		return Promise.resolve(`No product ID found for ID: ${reqParams.productId}`);
		}

	let updateActiveFields = {
		isActive : true
	};

	if(isAdmin){
		return Product.findByIdAndUpdate(reqParams.productId, updateActiveFields).then((product, error) => {

			// Product not archived
			if (error) {
				return false
				// false
			// Product archived successfully
			} else {
				return true
				//true
			}
		});	
	}

	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
};


// Change product quantities
module.exports.changeQuantityFields = (reqParams, reqBody, isAdmin) => {
	console.log(isAdmin);

	// Check if the provided productId is valid
	if (!mongoose.Types.ObjectId.isValid(reqParams.productId)) {
		return Promise.resolve(`No product ID found for ID: ${reqParams.productId}`);
		}

	if(isAdmin){
		let updateQuantityProduct = {
		stocks : reqBody.stocks
		}

		return Product.findByIdAndUpdate(reqParams.productId, {$inc: { stocks: reqBody.stocks },}).then((product, error) => {

			// Product not change quantity
			if (error) {
				return false
				// false
			// Product change quantity
			} else {
				return true
				//true
			}
		});	
	}

	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
};

// Delete specific product
  module.exports.deleteProduct = ( reqParams, isAdmin) => {
	console.log(isAdmin);

	// Check if the provided productId is valid
	if (!mongoose.Types.ObjectId.isValid(reqParams.productId)) {
		return Promise.resolve(`No product ID found for ID: ${reqParams.productId}`);
		}

	if(isAdmin){
		return Product.findByIdAndRemove(reqParams.productId).then((removedProduct) => {
			if (!removedProduct) {
			  return false
			}
			return true
		  })
	  };

	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
};


// Retreiving a specific category
module.exports.getCategory = (category) => {
	// Check if the provided category is valid
	if (typeof category !== 'string') {
		return Promise.resolve(`Error: Category must be a string`);
	} else {	
		return Product.find({ category: category, isActive : true}).then(result => {
			if (result.length === 0) {
				return Promise.resolve(`No product Category found for Category: ${category}`);
			} else {
				return Promise.resolve(result);
			}
		})
	}
}

// Retreiving all trending products
// Retrieving all trending products
module.exports.getTrending = (trending) => {
	console.log(trending); // check if the trending parameterwas passed correctly
	// Check if the provided trending is valid
	if (typeof trending !== 'boolean') {
	  return Promise.resolve(`Error: Trending must be a boolean`);
	} else {	
	  return Product.find({ trending: true, isActive: true }).then(result => {
		if (result.length === 0) {
		  return Promise.resolve(`No trending products found`);
		} else {
		  return Promise.resolve(result);
		}
	  })
	}
  }