const Order = require("../models/Order")
const Product = require("../models/Product")
const mongoose = require('mongoose');

// Creating user order
module.exports = {
  async saveOrder({ userId, items, isAdmin }) {
    if (isAdmin) {
      return "You don't have permission to perform this action"
    }

    const productPromises = items.map(async (item) => {
      const { productId, quantity } = item;
      const product = await Product.findById(productId);

      return {
        productId: product.id,
        name: product.name,
        category: product.category,
        price: product.price,
        description: product.description,
        quantity,
      };
    });

    const productDetails = await Promise.all(productPromises);

    const totalAmount = productDetails.reduce(
      (total, { quantity, price }) => total + quantity * price,
      0
    );

    const savedOrderItems = productDetails.map(({ productId, name, price, description, category, quantity }) => ({
      product: productId, name, description, category, price,
      quantity,
    }));
    
    const savedOrder = await Order.create({
      userId,
      products: savedOrderItems,
      totalAmount,
    });

    return savedOrder;
  },
};

// Get all user's order
module.exports.getOrdersForUser = async ( userId , isAdmin) => {
  console.log(isAdmin);

  if(!isAdmin){
    const orders = await Order.find({ userId  });
    if (orders) {
      return orders;
    } else {
      return false;
    }
  
    };

  // If the user is not admin, then return this message as a promise to avoid errors
  let message = Promise.resolve("You don't have the access rights to do this action.");

  return message.then((value) => {
    return value
  })
};

module.exports.getOrdersForAllUser = async () => {
  const orders = await Order.find({});
  if (orders) {
    return orders;
  } else {
    return false;
  }
};

// Delete specific order

module.exports.deleteOrder = ( reqParams, isAdmin) => {
  console.log(isAdmin);

  // Check if the provided orderId is valid
  if (!mongoose.Types.ObjectId.isValid(reqParams.orderId)) {
    return Promise.resolve(`No product ID found for ID: ${reqParams.orderId}`);
  }

  if(!isAdmin){
    return Order.findByIdAndRemove(reqParams.orderId).then((removedOrder) => {
      if (!removedOrder) {
        return false
      }
      return true
      })
    };

  // If the user is not admin, then return this message as a promise to avoid errors
  let message = Promise.resolve("You don't have the access rights to do this action.");

  return message.then((value) => {
    return value
  })
};


// Get all user's order
module.exports.getOrdersById = (reqParams, isAdmin) => {
  console.log(isAdmin);

  // Check if the provided orderId is valid
  if (!mongoose.Types.ObjectId.isValid(reqParams.orderId)) {
    return Promise.resolve(`No product ID found for ID: ${reqParams.orderId}`);
  }

  // Find the order by ID
  return Order.findById(reqParams.orderId).then((order) => {
    if (!order) {
      return false
    }

    // Check if the user is authorized to view the order
    if (!isAdmin && order.userId !== reqParams.userId) {
      return false
    }

    // Return the order information
    return order;
  });
};