// console.log("hello");

// Section if, else, else if statement
// Conditional statement
//  They allow us to control the flow of our program. allow us to run a statement or instruction if a condition met

let numA = -1;
// if statement
// execute a statement if specify condition is true
/* 
 Syntax 

 if(condition){
	codeblock or statement
 }
 */

if(numA < 0){
	console.log("hello");
}

// Basic Checking
console.log(numA < 0);


// lets update numA and run our system
numA = 0;

if(numA < 0){
console.log("hello again id numA is 0");

}


//another example
let city = "NY";
if(city === "NY");
console.log("Welcome to NY");

// else if Clause
/* 
    - Executes a statement if previous conditions are false and if the specified condition is true
    - The "else if" clause is optional and can be added to capture additional conditions to change the flow of a program

*/

let numH = 1;
if(numA < 0){
	console.log("hello");

} else if (numH > 0){
	console.log("world");
}

numA = 1;
if(numA > 0){
	console.log("hello");

} else if (numH > 0){
	console.log("world");
}else if (numH < 0){
	console.log("again");
}

// else statement
/* 
    - Executes a statement if all other conditions are false
    - The "else" statement is optional and can be added to capture any other result to change the flow of a program
*/
if(numA < 0){
	console.log("hello");
} else if (numH === 0){
	console.log("world");
}else{
	console.log("again");
}

/*
we cant use else statement alone

else{
	console.log("test");
}*/


/*
we cant use else statement alone. should have an "if" in the statement

else if (numH === 0){
	console.log("world");
}else{
	console.log("again");
}*/

/* structure - if, else if, else*/

// if, else if, else statement with fuctions

let message = "No message.";
console.log(message);

function determineTyphoonIntensity(windSpeed){
	if (windSpeed < 30){
		return "Not a typhoon yet";
	}
	else if(windSpeed <= 61){
		return "Tropical Depression detected.";
	} else if(windSpeed >= 62 && windSpeed <= 88){
		return "Tropical Storm detected.";
	}else if(windSpeed >= 89 && windSpeed <= 117){
		return "sever tropical storm detected.";
	}else{
		return "typhoon detected.";
	}


}
message = determineTyphoonIntensity(75);
console.log(message);

if(message == "Tropical Storm detected.");
{
	console.error(message);
	// console.warning(message);
}

// Thruthy anf falsy
// truthy

if(true){
	console.log("Truthy");
}

if(1){
	console.log("Truthy");
}
if([]){
	console.log("Truthy");
}


// falsy
if(false){
	console.log("falsy");
}
if(0){
	console.log("falsy");
}
if(undefined){
	console.log("falsy");
}

// section conditional (Ternary) Operator

/* 
    - The Conditional (Ternary) Operator takes in three operands:
        1. condition
        2. expression to execute if the condition is truthy
        3. expression to execute if the condition is falsy
    - Can be used as an alternative to an "if else" statement
    - Ternary operators have an implicit "return" statement meaning that without the "return" keyword, the resulting expressions can be stored in a variable
    - Commonly used for single statement execution where the result consists of only one line of code
    - For multiple lines of code/code blocks, a function may be defined then used in a ternary operator
    - Syntax
        (expression) ? ifTrue : ifFalse;
*/

// Single statement execution
let ternaryResult = (1 > 18) ? true : false;
console.log("Result of iternary operator: " + ternaryResult);


// Multiple statement execution
let name;
function isOfLegalAge(){
	name = "John";
	return "Your are of the legal age";
}
function isOfUnderAge(){
	name = "Joh";
	return "Your are under age limit";
}

let age = parseInt(prompt("What is Your age"));
console.log(age);
let legalAge = (age >= 18) ? isOfLegalAge() : isOfUnderAge();
console.log("Result of ternary operator: " + legalAge + ", " + name);

// section Switch the Statement
/*
Syntax
switch(expression){
	case value:
		statement
		break;
		default;
			bvrak;
}
*/

let day = prompt("what day of the week is it today?").toLowerCase();
console.log(day);

switch (day){
case "monday":
	console.log("The color of the day is red.");
	break;
	case "Tuesday":
	console.log("The color of the day is orange.");
	break;
	case "wednesday":
	console.log("The color of the day is yellow.");
	break;
	case "thursday":
	console.log("The color of the day is green.");
	break;
	case "friday":
	console.log("The color of the day is blue.");
	break;
	case "saturday":
	console.log("The color of the day is white.");
	break;
	case "sunday":
	console.log("The color of the day is violet.");
	break;
default:
	console.log("please input a valid day.")
	break;
}
// section Try-Catch-Finally
function showIntensityAlert(windSpeed){
	try{
		alerat(determineTyphoonIntensity(windSpeed));
	}catch (error){
		console.log(typeof error);
		console.warn(error.message);

	}finally{
		// continue the execution of the code
		alert("Intensity updates will show new alert.");

	}
}

showIntensityAlert(56);