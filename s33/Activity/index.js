// console.log("hello")

/*fetch("https://jsonplaceholder.typicode.com/todos")
.then(res => res.json())
.then(json => console.log(json));*/

fetch("https://jsonplaceholder.typicode.com/todos")
.then(res => res.json())
.then(title => {
	let infoMap = title.map(dataToMap => {
		return{
			title: dataToMap.title
		};
	});
	console.log(infoMap);
});



fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(res => res.json())
.then(res => console.log(res));




fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(res => res.json())
.then(user => console.log(`The item "${user.title}" on the list has a status of "${user.completed}"`))




fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({


		title: "created To do List Item",
		completed: false,
		userId: 1
	})
})
.then(res => res.json())
.then(dat => console.log(dat));




fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({
		
		title: "Updated To Do List Item",
		dateComplete: "pending",
		status: "Pending",
		description: "To update the my to do list with a defferent data structure",
		userId: 1


	})
})
.then(res => res.json())
.then(json => console.log(json));






fetch("https://jsonplaceholder.typicode.com/todos/2", {
	method: "PATCH",
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({
		dateComplete: "7/24/2000",
		status: "Completed"

	})
})
.then(res => res.json())
.then(update => console.log(update));






fetch("https://jsonplaceholder.typicode.com/todos/4", {
	method: "DELETE"
});



