// Section Function
// Functions in javascript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked
	// Functions are mostly created to create complicated tasks to run several lines of code in succession
	// They are also used to prevent repeating lines/blocks of codes that perform the same task/function


// Function Keyword - used defined a js function
// functionName - Function name are able to use later in the code
// function block ({}) - statement which com
/*
functioName(){
	//code blocked 
}
*/

console.log("hello");

function printName(){
	console.log("My name is john");
}
// Invocation or function calling
printName();
printName();
printName();


// Function Invocation
//The code block and statements inside a function is not immediately executed when the function is defined. The code block and statements inside a function is executed when the function is invoked or called.
	//It is common to use the term "call a function" instead of "invoke a function".

// printName(); - This is function invocation

// declaredFunction(); - will result of error because not declared

// Hoisting calling the fuction before the declaration
declaredFunction();

function declaredFunction(){
	console.log("Hello world from declaredFunction()");
}
declaredFunction();


// Function expression
//A function can also be stored in a variable. This is called a function expression.

		//A function expression is an anonymous function assigned to the variableFunction

		//Anonymous function - a function without a name.

let variableFunction = function(){
	console.log("hello again");
}

variableFunction();

/*
/ We can also create a function expression of a named function.
		// However, to invoke the function expression, we invoke it by its variable name, not by its function name.
		// Function Expressions are always invoked (called) using the variable name.*/

let funcExpression = function funcName(){
	console.log("Hello other side");
}
// funcName() will result error
funcExpression();

// reassignning or update a function expression
declaredFunction = function()
{
	console.log("Updated declaredFunction");
}
declaredFunction();

funcExpression = function(){
	console.log("Updated funcExpression");
}
funcExpression();
funcExpression();

const contantFunction = function(){
	console.log("Initialized with const");
}
contantFunction();

/*contantFunction = function(){
	console.log("Cant be updated");
}
contantFunction(); - result of an error cant update it.

*/

// Function Scoping
/*	
	Scope is the accessibility (visibility) of variables within our program.
	
	Javascript Variables has 3 types of scope:
		1. local/block scope
		2. global scope
		3. function scope
*/

function showNames(){
	// Function scope variables
	var functionVar = "Joe";
	const functionConst = "john";
	let functionlet = "jane";

	console.log(functionVar);
console.log(functionConst);
console.log(functionlet);
}
 showNames();

 /*console.log(functionVar);
console.log(functionConst);
console.log(functionlet); - will result an error cant read out in {} or function*/

let globalVar = "Mr. world";


 {
 	let localVar = "Armando perez";
 	console.log(localVar); // this is work because the declaration and variable in inside the {}

 	console.log(globalVar);
 }
 
 // console.log(localVar) - error if not inside the {}

 // Nested Function
 function myNewFunction(){
 	let name = "hane";
 	function nestedFucntion(){
 		let nestedName = "john";
 		console.log(name);
 	}
 	// console.log(nestedName); - will result an error because not inside the {} of nestedname
 	nestedFucntion();
 }
 myNewFunction();
 // nestedFucntion(); - will result an error



 // Function and Global scope variables
 // Global variables
let globalName = "alex";
function myNewFunction2(){
	let nameInside = "renz";
	console.log(globalName);
	// global variables can access inside the function

}
myNewFunction2();


// section return Statement
//  to use return statement will using return the keyword

function returnFullName(){

	return "Jeffrey";
	// console.log("Hello");
	// console.log("Hello");
	// console.log("Hello"); will never run becuase the return statement was declared before this line of codes.

}
let fullName = returnFullName();
console.log(fullName);

console.log(returnFullName());

function returnFullAddress(){
	let fullAddress = {
		street: "#44 Maharlika",
		city: "Cainta",
		province: "Rizal"
	}
	return fullAddress;
}

let MyAddress = returnFullAddress();
console.log(MyAddress);

function printPlayerInfo(){
	console.log("Username: " + "white_knight");
	console.log("Level: " + 95);
	console.log("job: " + "paladin");
}

let user1 = printPlayerInfo();
console.log(user1);

// you can return any data types from a function
function returnSumOf5and10(){
	return 5 + 10;

}
 let sumOf5and10 = returnSumOf5and10();
 console.log(sumOf5and10);

 let total = 100 + returnSumOf5and10();
console.log(total);

// Simulates getting an array of user names from a DB

function getGuildMembers(){
	return ["white_knight", "healer2000", "masterthief100"];
}

console.log(getGuildMembers());

// Function Naming Convention
// Function names shoul be definitive of the task it will perform.

function getCourses(){
	let courses = ["Science", "Math", "English"];

	return courses;
}

let courses = getCourses();
console.log(courses);

// Avoid generic names

function get(){

}

function foo(){
	
}
