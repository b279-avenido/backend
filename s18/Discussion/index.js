// Function
// Parameters And arguments

// Functions in javascript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked
			// Functions are mostly created to create complicated tasks to run several lines of code in succession
			// They are also used to prevent repeating lines/blocks of codes that perform the same task/function

			//We also learned in the previous session that we can gather data from user input using a prompt() window.

// function printInput(){
// 	let nickName = prompt("Enter your nickname");
// 	console.log("hi, " + nickName);
// }

// printInput();

// Complete function with parameter and argument
function printName(name){
	console.log("My name is " + name);

}
printName("Juan");
printName("john");


// parameter - name
// its acts a variable or container
// can store data in our parameter
//  Argument is "juan"
// located in the invocation or calling
// "juan" will be stored in our parameter "name"\


let sampleVariable = "Yui";

printName(sampleVariable);

function checkDivisibilityBy8(num){
let remainder = num % 8;
				console.log("The remainder of " + num + " divided by 8 is: " + remainder);
				let isDivisibleBy8 = remainder === 0;
				console.log("Is " + num + " divisible by 8?");
				console.log(isDivisibleBy8);	
}

checkDivisibilityBy8(64);


// function as arguments
function argumentFunction(){
	console.log("this is function was passed");
}

function invokeFunction(argumentFunction){
	argumentFunction();
}

invokeFunction(argumentFunction);
console.log(argumentFunction);

// Mutiple parameters
function createFullName(firstName, lastName, middle){
	console.log(firstName + " " + middle + " " + lastName)
}
createFullName("juan", "ella", "miya");
// "Juan" will be stored in the parameter "firstName"
// "miya" will be stored in the parameter "middleName"
// "ella" will be stored in the parameter "lastName"

createFullName("juan", "miya");
createFullName("juan", "ella", "miya", "hello");

// Using variables as arguments
let firstName = "john", middle = "foe", lastName = "smith"
createFullName(firstName, middle, lastName);

// Argument order
function printFullName(middle, firstName, lastName){
  console.log(firstName + " " + middle + " " + lastName);
}
printFullName("juan", "dela", "nice");

// Using alert()
// alert() - allows us to show small window at the top of our broswer to show in

alert("hello");

function showSampleAlert(){
	alert("hello, user");
}
showSampleAlert();
console.log("i will only log in the console");

//Notes on the use of alert():
	//Show only an alert() for short dialogs/messages to the user. 
	//Do not overuse alert() because the program/js has to wait for it to be dismissed before continuing.


function printWelcomeMessage(){
	let firstName = prompt("Enter your first name");
	let lastName = prompt("Enter your last name");

	console.log("Hello, " + firstName + " " + lastName + "!");
	console.log("Welcome to my page!");
}

printWelcomeMessage();
