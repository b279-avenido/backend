db.hotel.insertOne({

	name: "single",
	accommodates: 2,
	price: 1000,
	description: "A simple room with all the basic necessities",
	rooms_available: 10,
	isAvailable: false
});


db.hotel.insertMany([{
	name: "double",
	accommodates: 3,
	price: 2000,
	description: "A room fot for a small family going on a vacation",
	rooms_available: 5,
	isAvailable: false
},
{
	name: "queen",
	accommodates: 4,
	price: 4000,
	description: "A room with a queen sized bed perfect for a simple getaway",
	rooms_available: 15,
	isAvailable: false
}]);


/*db.hotel.insertMany([{
	name: "queen",
	accommodates: 4,
	price: 4000,
	description: "A room with a queen sized bed perfect for a simple getaway",
	rooms_available: 15,
	isAvailable: "false"
}]);*/

db.hotel.find({name: "double"});


db.hotel.updateOne(
{name: "queen"}, 
{
	$set: {name: "queen",
	rooms_available: 0
	
}
});

db.hotel.deleteMany({
	rooms_available: 0
});

db.hotel.find();