// CRUD operation
/*
    - CRUD operations are the heart of any backend application.
    - Mastering the CRUD operations is essential for any developer.
    - This helps in building character and increasing exposure to logical statements that will help us manipulate our data.
    - Mastering the CRUD operations of any language makes us a valuable developer and makes the work easier for us to deal with huge amounts of information.
*/

// section - C - inserting documents or create
// insert one ducement
/*
    - Since mongoDB deals with objects as it's structure for documents, we can easily create them by providing objects into our methods.
    - The mongo shell also uses JavaScript for it's syntax which makes it convenient for us to understand it's code
    - Creating MongoDB syntax in a text editor makes it easy for us to modify and create our code as opposed to typing it directly in the terminal where the whole code is only visible in one line.
- By using a text editor it allows us to type the syntax using multiple lines and simply copying and pasting the code in terminal will make it work.
    - Syntax
        - db.collectionName.insertOne({object});
    - JavaScript syntax comparison
        - object.object.method({object});
*/

db.users.insertOne({
	firstName: "jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "12345678910",
		email: "janedoe@gmail.com"
	},

	courses: ["CSS", "Javascript", "phyton"],
	department: "none"
});

// insert many
/*
syntax
db.collectName.insertMany([
{object}, {objectA}
]);

*/

db.users.insertMany([
	{
	firstName: "stephen",
	lastName: "Hawking",
	age: 76,
	contact: {
		phone: "12345678910",
		email: "stephenhawking@gmail.com"
	},

	courses: ["python", "react", "PHP"],
	department: "none"
},
{
	firstName: "Neil",
	lastName: "armstrong",
	age: 82,
	contact: {
		phone: "12345678910",
		email: "neilarmstrong@gmail.com"
	},

	courses: ["react", "laravel", "Sass"],
	department: "none"
}

	]);

// Finding a single document
// leaving the search criteria empty will retrive all documents
// select  *

db.users.find();

// select * from users where firstname = "stephen" in sql

db.users.find({ firstName: "stephen" });

// the "pretty" method allow us to be able to view document returned by our terminal in a better format

db.users.find({ firstName: "stephen" }).pretty();

// finding a documents with a multiple parameters
db.users.find({ lastName: "armstrong" }, { age: 82 });

// Section - U - Updating documents
// updating single doc
// creating a dummy user to update

db.users.insertOne({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "00000000000",
		email: "test@gmail.com"
	},

	courses: [],
	department: "none"
});

/*
syntax
db.collectionName.updateOne({criteria}, {$set: {field: value}});


*/

db.users.updateOne(
	{ firstName: "Test" },
	{
		$set: {
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "09123456789",
				email: "bill@mail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Operations",
			status: "active"
		}
	}
);

db.users.find({ firstName: "Bill" });



// updating many documents
db.users.updateMany(
	{ department: "none" },
	{ 
		$set: {
			department: "HR"
		}
	 }

	);

//  replace one

/*
	syntax
	db.collectionName.replaceOne({criteria}, {field: value});

*/

db.users.replaceOne(
 { firstName: "Bill"},
 { 
 	firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "09123456789",
				email: "bill@mail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Operations",
			
 }
  

	);


// section - D - deleting documents

db.users.insertOne({
	firstName: "test"
});

// delete many or multiple 
db.users.deleteMany({
	firstName: "test"
});

// delete one
db.users.deleteOne({
	firstName: "test"
});


// section - advance queries

db.users.find({
	contact: {
		phone: "12345678910",
		email: "stephenhawking@gmail.com"
	}
});

// query on nested field
db.users.find({
	"contact.mail": "stephenhawking@gmail.com"
});

// quering an array 

/*
syntax
db.users.find()
*/
db.users.find({courses: {$all: ["react", "Phyton"]}});
db.users.find({courses: {$all: ["react"]}});

// Querying and embeded array

db.users.insertOne({
	nameArray: [{nameA: "Juan"}, {nameB: "tamad"}]
});