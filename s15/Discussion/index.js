message = 'John\'s employees went home';
console.log(message);

// Numbers
// Intergers/Whole Numbers

let headcount = 26;
console.log(headcount);

// Decimal Numbers/Fractions
let grade = 98.7;
console.log(grade);

// Exponential Notation
let planetDistance = 2e20;
console.log(planetDistance);

// Combining of text and int
console.log("John's grade last quarter is " + grade);

// Boolean
// Boolean values are normally used to store values relating to the state of certain thing.
let isMarried = false;
let inGoodConduct = true;

console.log("isMarried? " + isMarried);
console.log("inGoodConduct? " + inGoodConduct);

// Arrays
// Arrays are a special kind of data type that's used to store multiple values.
// Arrays can store different data types but is normally used to store similar data types.
// Syntax let/const arrayName = [valueA, valueB, valueC...];

let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

// different data types
let details = ["John", "Smith", 32, true];
console.log(details);

// Objects
// Objects are another special kinds of data type
// It has properties and values -> key pairs
/*

let/const objectName = {
    propertyA: value,
    propertyB: value
}

*/

let person = {
    fullName: "Juan Dela Cruz",
    age: 35,
    isMarried: false,
    contact: ["+6312345", "+6398765"],
    address: {
        houseNumber: "345",
        city: "Manila"
    }
}

console.log(person);

// They're also useful for creating abstract objects
let myGrades = {
    firstGrading: 98.7,
    secondGrading: 92.1,
    thirdGrading: 98.2,
    fourthGrading: 94.6
}

console.log(myGrades);

// "typeof" is used to determine the type of data of a variable.
console.log(typeof isMarried);
console.log(typeof person);
console.log(typeof grades);

// Constant Object and Arrays

const anime = ["one piece", "once punch man", "attack on titan"];
anime[0] = "Kimetsu No Yaiba";
console.log(anime);

// Null
// It is used to intentionally express the absence of a value;
// This means the variable does not hold any value;
// Null = 0 = ""

let spouse = null;
let myNumber = 0;
let myString = "";
console.log(spouse);

// Undefined
// Represents the state of a variable that has been declared withoud initial value

let fullName;
console.log(fullName);
